Pod::Spec.new do |s|
    s.name         = "ProtoChat"  
    s.version      = "0.1.0"
    s.summary      = "iOS library for easy integration of Proto based Chatbot."
    s.description  = <<-DESC
    Library will provide a chat window with UI and action buttons to chat with bot designed with Proto platform.
    DESC
    s.homepage     = "http://proto.cx"          
    s.license = { :type => 'Copyright', :text => <<-LICENSE
                   Copyright 2021 Proto
                  LICENSE
                }
    s.author             = { "Tuan" => "tuan@proto.cx" }
    s.source       = { :git => 'https://gitlab.com/protocx/iosprotospecs.git', :tag => s.version.to_s }
    s.vendored_frameworks = "ProtoChat.xcframework"
    s.platform = :ios      
    s.swift_version = "4"            
    s.ios.deployment_target = '12.0'
    s.dependency 'Alamofire', '~> 4.9.1'
    s.dependency 'ObjectMapper', '~> 3.4'
    s.dependency 'AlamofireObjectMapper', '~> 5.2.1'
    s.dependency 'SnapKit', '~> 4.0.0'
    s.dependency 'ReverseExtension'
    s.dependency 'Firebase/Messaging'
    s.dependency 'SwiftEventBus', '~> 3.0.1'
end
